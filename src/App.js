import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
import NetInfo from "@react-native-community/netinfo";

export class App extends Component {

  NetInfoSubscription = null;

  constructor(){
    super();
    this.state = {
        connectionStatus: false,
    }
  }    

  componentDidMount(){
    this.NetInfoSubscription = NetInfo.addEventListener(
      this.handleConnectivityChange,
    )
  }

  componentWillUnmount(){
    this.NetInfoSubscription && this.NetInfoSubscription();
  }

  handleConnectivityChange = (state) => {
    this.setState({ connectionStatus: state.isConnected})
  }

  render() {
    return (

      <NavigationContainer>

        {this.state.connectionStatus ? null : 
          <View style={{backgroundColor: 'red'}}>
              <Text style={{textAlign: 'center', color: 'white', paddingVertical: 5}}>
                  {/* Connection Status: {this.state.connectionStatus ? 'Connected' : 'Disconnected'} */}
                  You are offline
              </Text>
          </View>
        }

        <Router />    
      </NavigationContainer>
    )
  }
}

export default App
