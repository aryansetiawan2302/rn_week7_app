// import { StackActions } from '@react-navigation/native';
import React, { Component } from 'react'
import { Text, View, ImageBackground, StyleSheet, Image } from 'react-native'
import { SplashBackground, Logo } from '../../assets';

class Splash extends Component {

    constructor(props){
        super(props);
        this.state = {};
    }

    componentDidMount() {
        setTimeout(()=>{
            this.props.navigation.replace('Home')
            // this.props.navigation.dispatch(StackActions.replace('Home'));
        }, 3000);
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={SplashBackground} style={styles.background}>
                    <Image source={Logo} style={styles.logo}/>
                </ImageBackground>
                <Text style={styles.name}>Aryan</Text>
            </View>
        )
    }
}

export default Splash

const styles = StyleSheet.create({
    container: {
        flex: 1, 
    },
    background: {
        flex: 1, // pushes the footer to the end of the screen
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 222,
        height: 205,
    },
    name: {
        color: 'darkgrey',
        height: 30,       
        textAlign: 'center',
    }
})