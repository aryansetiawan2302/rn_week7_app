import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet, 
    Image, 
    Dimensions, 
    ScrollView, 
    TouchableOpacity, 
    RefreshControl,
    ActivityIndicator,
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import Share from 'react-native-share';
// import { Avatar } from '../../assets';

export class MovieDetail extends Component {

    constructor(props){
        super(props)
        this.state = {
            dataMovieDetail: {},
            dataGenre: [],
            dataCast: [],
            isLoading: false,
            refreshing: false,
        }
    }

    async componentDidMount(){

        this.setState({isLoading: true})
        console.log(this.props.route.params.movieID)

        const dataMovieDetailDariServer = await axios.get(`http://code.aldipee.com/api/v1/movies/${this.props.route.params.movieID}`)
        // console.log(dataMovieDetailDariServer.data.title)
        this.setState({dataMovieDetail: dataMovieDetailDariServer.data})
        // console.log(this.state.dataMovieDetail.genres)
        this.setState({dataGenre: this.state.dataMovieDetail.genres})
        this.setState({dataCast: this.state.dataMovieDetail.credits.cast})
        // console.log(this.state.dataMovieDetail.credits.cast)
        this.setState({isLoading: false})

        // console.log(dataMovieDetail.title)
    }

    onRefresh = async () => {
        this.setState({refreshing: true});
        const hasilRefresh = await axios.get(`http://code.aldipee.com/api/v1/movies/${this.props.route.params.movieID}`)
        this.setState({dataMovieDetail: hasilRefresh.data})
        this.setState({dataGenre: this.state.dataMovieDetail.genres})
        this.setState({dataCast: this.state.dataMovieDetail.credits.cast})
        this.setState({refreshing: false});
    }

    onShare = async () => {
        const shareOptions = {
            message: 'This is a test message'
        }

        try {
            const ShareResponse = await Share.open(shareOptions)
        } catch (error) {
            console.log('Error => ', error)
        }
    }
    
    render() {
        return (
            <ScrollView
                contentContainerStyle={styles.contentContainer}
                refreshControl={
                    <RefreshControl 
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                    />
                }
            >

            {this.state.isLoading ? (
                <View style={styles.loading}>
                    <ActivityIndicator size='large' color='#9D6F23'/>
                </View>
                // console.log('Loading, tunggu dulu ...') : 
            ) :
    
            <View>


                <Image 
                    style={styles.poster}
                    source={{
                        uri: `${this.state.dataMovieDetail.backdrop_path}`,
                    }}
                />
                <TouchableOpacity 
                    style={styles.topIconBack}
                    onPress={()=>{
                        this.props.navigation.navigate('Home')
                    }}                                        
                >
                <Icon name="arrow-back" size={30} color="#9D6F23"/>
                </TouchableOpacity>
                
                <TouchableOpacity 
                    style={styles.topIconFav}
                >
                    <Icon name="heart-outline" size={30} color="#9D6F23"/>
                </TouchableOpacity>

                <TouchableOpacity 
                    style={styles.topIconShare}
                    onPress={this.onShare}                >
                <Icon name="arrow-redo-outline" size={30} color="#9D6F23"/>
                </TouchableOpacity>

                <View style={styles.container}>

                    <Image 
                        style={styles.smallPoster}
                        source={{
                            uri: `${this.state.dataMovieDetail.poster_path}`,
                        }}
                    />
                    <View style={styles.detailMovieList}>
                        <View style={{flex: 1}}>
                            <Text style={styles.textJudulFilm}>{this.state.dataMovieDetail.title}</Text>
                            <Text style={styles.textLain}>{this.state.dataMovieDetail.tagline}</Text>
                            <Text style={styles.textLain}>{this.state.dataMovieDetail.status}</Text>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                            <View style={{alignItems: 'center'}}>
                                <Icon name="calendar-sharp" size={25} color="#9D6F23"/>
                                <Text style={styles.textLain}>{this.state.dataMovieDetail.release_date}</Text>
                            </View>
                            <View style={{alignItems: 'center'}}>
                                <Icon name="star" size={25} color="#9D6F23"/>
                                <Text style={styles.textLain}>{this.state.dataMovieDetail.vote_average}</Text>
                            </View>
                            <View style={{alignItems: 'center'}}>
                                <Icon name="stopwatch-outline" size={25} color="#9D6F23"/>
                                <Text style={styles.textLain}>{this.state.dataMovieDetail.runtime} menit</Text>
                            </View>
                        </View>
                    </View>

                </View>

                <View style={styles.bottomContainer}>
                    <Text style={styles.textTitle}> Genres </Text>

                    <View style={styles.genreList}>
                        {this.state.dataGenre.map((item)=> (
                            <View style={styles.genreItem}>
                                <Text key={item.id} style={styles.textGenre}>{item.name}</Text>
                            </View>
                        ))}
                    </View>

                    <Text style={styles.textTitle}> Synopsis </Text>
                    <Text style={styles.textSynopsis}>
                        {this.state.dataMovieDetail.overview}
                    </Text>

                    <Text style={styles.textTitle}> Actors/Artist </Text>

                    <View style={styles.containerActorList}>

                        {this.state.dataCast.map((item)=> (
                        // <Text style={styles.textLain}>Test</Text>

                            <View key={item.id} style={styles.containerActor}>
                                <Image 
                                    style={styles.photo}
                                    // source={`${item.profile_path}` ? 
                                    //     {uri: `${item.profile_path}`} :
                                    //     {Avatar}                                    
                                    // }

                                    source={{
                                        uri: `${item.profile_path}`,
                                    }}
                                />
                                <Text style={styles.textLain}>{item.name}</Text>
                            </View>
                        ))}

                    </View>
                </View>

            </View>

            }

            </ScrollView>
        )
    }
}

export default MovieDetail

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

    contentContainer: {
        backgroundColor: 'lightgrey',
        paddingBottom: 50,
    },
    loading: {
        width: windowWidth,
        height: windowHeight,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    poster: {
        width: windowWidth,
        height: 281*0.8,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
    },
    topIconBack: { 
        position: 'absolute', 
        top: 20, 
        left: 10, 
        padding: 3, 
        backgroundColor: '#FEC60E', 
        borderRadius: 20, 
    },
    topIconFav: { 
        position: 'absolute', 
        top: 20, 
        right: 50, 
        padding: 3, 
        backgroundColor: '#FEC60E', 
        borderRadius: 20,
        opacity: 0.75,
    },
    topIconShare: { 
        position: 'absolute', 
        top: 20, 
        right: 10, 
        padding: 3, 
        backgroundColor: '#FEC60E', 
        borderRadius: 20, 
        opacity: 0.75,
    },

    smallPoster: {
        width: 500*0.2,
        height: 750*0.2,
        marginRight: 15,
        borderRadius: 10,
    },
    container: {
        backgroundColor: 'white',
        padding: 17,
        marginHorizontal: 30,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        marginTop: windowHeight*-0.07,
        flexDirection: 'row',
    },
    detailMovieList: {
        flex: 1,
    },
    textJudulFilm: {
        fontSize: 25,
        fontFamily: 'LilitaOne-Regular'
    },
    textLain: {
        fontSize: 13,
        fontFamily: 'JosefinSans-Regular'
    },


    bottomContainer: {
        padding: 10,
    },
    textTitle: {
        marginTop: 20,
        marginBottom: 10,
        fontSize: 18,
        fontFamily: 'JosefinSans-Bold',
    },
    genreList: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginRight: 10,
        flexWrap: 'wrap',
    },
    textGenre: {
        fontSize: 12,
        color: 'white',
        textAlign: 'center',
        fontFamily: 'JosefinSans-SemiBoldItalic'
    },
    genreItem: {
        backgroundColor: 'grey',
        padding: 5,
        margin: 3,
        borderRadius: 5,
    },
    textSynopsis: {
        fontSize: 12,
        fontFamily: 'JosefinSans-Regular'
    },

    containerActorList: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginHorizontal: 10,
        flexWrap: 'wrap',
    },
    containerActor: {
        alignItems: 'center',
        marginBottom: 10,
    },
    photo: {
        width: 500*0.2,
        height: 748*0.2,
        marginHorizontal: 5,
        borderRadius: 15,
    },

})
