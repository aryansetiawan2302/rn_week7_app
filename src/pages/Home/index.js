import React, { Component } from 'react'
import { 
    Text, 
    View, 
    Image, 
    StyleSheet, 
    TouchableOpacity, 
    ScrollView, 
    RefreshControl, 
    ActivityIndicator,
    Dimensions
} from 'react-native'
import axios from 'axios'

export class Home extends Component {

    constructor(){
        super();
        this.state = {
            dataMovieList: [],
            refreshing: false,
            isLoading: false,
        }
    }    

    async componentDidMount(){

        this.setState({isLoading: true})
        const hasilFetching = await axios.get('http://code.aldipee.com/api/v1/movies')
        // console.log(hasilFetching.data.results)
        this.setState({dataMovieList: hasilFetching.data.results})
        this.setState({isLoading: false})
    }

    onRefresh = async () => {
        this.setState({refreshing: true});
        // setTimeout(()=>{
        //     console.log('refreshing');
        //     this.setState({refreshing: false});
        //     Alert.alert('Refresh selesai')
        // }, 2000);
        const hasilRefresh = await axios.get('http://code.aldipee.com/api/v1/movies')
        this.setState({dataMovieList: hasilRefresh.data.results})
        this.setState({refreshing: false});
    }

    convertGenre(genre_id) {
        switch (genre_id){
            case    12      : return 'Adventure';
            case    14      : return 'Fantasy';
            case    16      : return 'Animation';
            case    18      : return 'Drama';
            case    27      : return 'Horror';
            case    28      : return 'Action';
            case    35      : return 'Comedy';
            case    37      : return 'Western';
            case    53      : return 'Thriller';
            case    80      : return 'Crime';
            case    878     : return 'Science Fiction';
            case    9648    : return 'Mystery';
            case    10751   : return 'Family';
            default         : return 'Others';
        }
    }

    render() {
        return (

            <ScrollView
                style={styles.scrollView} 
                contentContainerStyle={styles.contentContainer}
                refreshControl={
                    <RefreshControl 
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                    />
                }
            >

            {this.state.isLoading ? (
                <View style={styles.loading}>
                    <ActivityIndicator size='large' color='orange'/>
                </View>
            ): 
        
            <View>
                <Text style={styles.textTitle}> Recommended </Text>


                <ScrollView horizontal={true}>
                    <View style={styles.containerRecommended}>
                        {this.state.dataMovieList
                            .sort((a, b) => a.vote_average < b.vote_average ? 1 : -1)
                            .map((item)=> (
                                <View key={item.id}>
                                    <TouchableOpacity
                                        onPress={()=>{
                                            this.props.navigation.navigate('MovieDetail', {movieID: item.id})
                                        }}                                        
                                    >
                                    <Image 
                                        style={styles.poster}
                                        source={{
                                            uri: item.poster_path,
                                        }}
                                    />
                                    </TouchableOpacity>
                                </View>
                        ))}
                    </View>
                </ScrollView>

                <Text style={styles.textTitle}> Latest Upload </Text>

                {this.state.dataMovieList
                    .sort((a, b) => new Date(`${b.release_date}`) - new Date(`${a.release_date}`))
                        
                    .map((item)=> (

                    <View key={item.id} style={styles.containerMovieList}>
                        <Image 
                            style={styles.smallPoster}
                            source={{
                                uri: item.poster_path,
                            }}
                        />
                        <View style={styles.detailMovieList}>
                            <Text style={styles.textJudulFilm}>{item.original_title}</Text>
                            <Text style={styles.textLain}>Release Date: {item.release_date}</Text>
                            <Text style={styles.textLain}>Rating: {item.vote_average}</Text>
                            <View style={styles.genreList}>
                                {item.genre_ids.map((itemGenre)=> (
                                    // <Text style={styles.textLain}>{itemGenre} </Text>
                                    <View style={styles.genreItem}>
                                    <Text key={itemGenre} style={styles.textGenre}>{this.convertGenre(itemGenre)}</Text>
                                    </View>
                                ))}
                            </View>

                            <TouchableOpacity 
                                    style={styles.buttonMore}
                                    onPress={()=>{
                                        this.props.navigation.navigate('MovieDetail', {movieID: item.id})
                                    }}
                            >
                                    <Text style={styles.textButtonMore}>show more</Text>
                            </TouchableOpacity> 

                        </View>
                    </View>
                
                ))}

            </View>

            }
            </ScrollView>
        )
    }
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

    loading: {
        width: windowWidth,
        height: windowHeight,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textTitle: {
        marginTop: 30,
        marginBottom: 10,
        marginLeft: 10,
        fontSize: 20,
        fontFamily: 'LilitaOne-Regular',
    },
    containerRecommended: {
        flexDirection: 'row',
    },
    poster: {
        width: 500*0.3,
        height: 750*0.3,
        borderRadius: 15,
        marginHorizontal: 5,
    },

    containerMovieList: {
        flexDirection: 'row',
        marginVertical: 10,
    },
    smallPoster: {
        width: 500*0.25,
        height: 750*0.25,
        marginHorizontal: 20,
        borderRadius: 10,
    },
    detailMovieList: {
        flex: 1,
    },
    textJudulFilm: {
        fontSize: 22,
        marginBottom: 10,
        marginRight: 5,
        fontFamily: 'JosefinSans-Bold',
    },
    textLain: {
        fontSize: 15,
        marginBottom: 5,
        fontFamily: 'JosefinSans-Regular',
    },
    genreList: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginRight: 10,
        flexWrap: 'wrap',
    },
    textGenre: {
        fontSize: 10,
        color: 'white',
        textAlign: 'center',
        fontFamily: 'JosefinSans-SemiBoldItalic'
    },
    genreItem: {
        backgroundColor: 'grey',
        padding: 5,
        margin: 3,
        borderRadius: 5,
    },
    buttonMore: {
        marginTop: 10,
        backgroundColor: 'brown',
        width: 100,
        alignItems: 'center',
        borderRadius: 25,
    },
    textButtonMore: {
        color: 'white',
        padding: 5,
        fontFamily: 'JosefinSans-SemiBold'
    },

    scrollView: {
        height: '60%',
        padding: 10,
        backgroundColor: 'orange'
      },
    contentContainer: {
        backgroundColor: 'lightgrey',
        paddingBottom: 50
    }

});
