import Logo from './logo.png'
import SplashBackground from './splashBackground.png'
import Avatar from './avatar.jpg'

export { Logo, SplashBackground, Avatar }