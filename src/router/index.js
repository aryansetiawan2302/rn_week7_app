import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { Home, Splash, MovieDetail } from '../pages';

const Stack = createStackNavigator();

export class Router extends Component {
    render() {
        return (
            <Stack.Navigator initialRouteName="Splash">
                <Stack.Screen name="Home" component={Home} options={{headerShown: false}}/>
                <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}}/>
                <Stack.Screen name="MovieDetail" component={MovieDetail} options={{headerShown: false}}/>
            </Stack.Navigator>
          )
    }
}

export default Router
